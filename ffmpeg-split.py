import logging
import re
import subprocess
import sys
from argparse import ArgumentParser
from pathlib import Path


SIZE_UNITS = {
	"B":   1,
	"kB":  1000,
	"KB":  1000,
	"MB":  1000000,
	"GB":  1000000000,
	"TB":  1000000000000,
	"KiB": 1024,
	"MiB": 1024*1024,
	"GiB": 1024*1024*1024,
	"TiB": 1024*1024*1024*1024,
}


def main() -> None:
	# noinspection PyArgumentList
	logging.basicConfig(style='{', format="{asctime} {levelname:5} [{name}] {message}",
	                    stream=sys.stdout,
	                    level=logging.INFO)

	parser = ArgumentParser()
	parser.add_argument('--split-size', type=str, required=True, metavar="SIZE",
	                    help=f"""Split in files of given max size. Use units: {", ".join(SIZE_UNITS.keys())}. """
	                         f"""Example: "1.5 MiB".""")
	parser.add_argument('file_patterns', type=str, nargs='+', metavar="FILE_PATTERN",
	                    help="""Glob-style pattern of file names. Example: "video*.mp4".""")
	args = parser.parse_args()

	regex = re.compile(r"(\d+(?:\.\d+)?)\s?({0})".format("|".join(re.escape(unit) for unit in SIZE_UNITS.keys())))
	match = regex.fullmatch(args.split_size)
	if match is None:
		logging.error(f"Cannot parse the split size: {args.split_size}")
		return
	split_size = int(float(match.group(1)) * SIZE_UNITS[match.group(2)])
	logging.debug(f"Split size: {split_size} bytes")

	cwd = Path.cwd()
	for file_pattern in args.file_patterns:
		for input_path in cwd.glob(file_pattern):
			if input_path.stat().st_size <= split_size:
				logging.info(f"Skipping {input_path.relative_to(cwd)}")
				continue

			duration = probe_duration(input_path)
			logging.info(f"Splitting {input_path.relative_to(cwd)} ({duration} seconds total)")

			i = 1
			cur_pos = 0.0
			while duration - cur_pos > 0.5:
				output_path = input_path.with_stem(f"{input_path.stem}_{i:03}")
				logging.info(f"Writing {output_path.relative_to(cwd)} (starting from {cur_pos} second)")
				subprocess.check_call(["ffmpeg",
				                       "-y",
				                       "-v", "quiet",
				                       "-i", input_path,
				                       "-codec", "copy",
				                       "-ss", str(cur_pos),
				                       "-fs", str(split_size),
				                       output_path])

				cur_duration = probe_duration(output_path)
				logging.debug(f"Chunk duration: {cur_duration} seconds")

				i += 1
				cur_pos += cur_duration


def probe_duration(path: Path) -> float:
	out = subprocess.check_output(["ffprobe",
	                               "-i", path,
	                               "-show_entries", "format=duration",
	                               "-v", "quiet",
	                               "-of", "default=noprint_wrappers=1:nokey=1"])
	return float(out)


if __name__ == '__main__':
	main()
