Requirements
============

- Python 3.9+
- ffmpeg

## Python

On Windows:
    
    choco install python

## ffmpeg

On Windows:
    
    choco install ffmpeg


Usage
=====

    usage: ffmpeg-split.py [-h] --split-size SIZE FILE_PATTERN [FILE_PATTERN ...]
    
    positional arguments:
      FILE_PATTERN       Glob-style pattern of file names. Example: "video*.mp4".
    
    optional arguments:
      -h, --help         show this help message and exit
      --split-size SIZE  Split in files of given max size. Use units: B, kB, KB,
                         MB, GB, TB, KiB, MiB, GiB, TiB. Example: "1.5 MiB".
